#!/usr/bin/python3

import requests
import json

while (True):
    command = input()
    payload = {'request':command}

    r = requests.post('http://127.0.0.1:8000', data=json.dumps(payload))
    print(r.text)
