Middleware.py
    Command: python Middleware.py
        Start the middleware application listening to port 8000
        Middleware responds to HTTP POST requests
        POST Requests:
            {'request':'state'}
                Returns {'STATE':state} where state is one of the following
                    "DEPLOYING"
                    "DEPLOYED"
                    "STOWING"
                    "CHARGING"
                    "READY"
                    "ERROR"
            {'request':'deploy'}
                if currently in "READY" state, start deploy routine, return {'WAIT', 10}
                if currently in "DEPLOYING" state, continue deploying, return {'WAIT', 10}
                10 corresponds to DEPLOY_TIME variable
                if currently in "DEPLOYED" state, return {'DONE', 'DEPLOYED'}
                if currently in "STOWING" or "CHARGING", ignore, return {'ERROR', 'Cannot deploy'}

            {'request':'stow'}
                if currently in "DEPLOYED" state, start stow routine, return {'WAIT', 10}
                if currently in "STOWING" state, continue stowing, return {'WAIT', 10}
                10 corresponds to STOW_TIME variable
                if currently in "CHARGING" state, return {'DONE', 'CHARGING'}
                if currently in "READY" or "DEPLOYING", ignore, return {'ERROR', 'Cannot deploy'}

PACEmulator.py
    Command: python PACEmulator.py
        Starts a PAC emulator listening to port 8001
        To be used when IS_EMULATE = True in Middleware.py

LocalTestClient.py
    Command: python LocalTestClient.py
        Starts the test client with Middleware.py running locally
        Input:
            deploy
                Send a deploy request and print return message
            stow
                Send a stow request and print return message
            state
                Send a state request and print return message

EmulatorTestClient.py
    Command: python EmulatorTestClient.py
        Same as LocalTestClient.py but sends requests to emulator webserver (http://nuc.smbox.co:8000)
