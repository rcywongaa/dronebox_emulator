#!/usr/bin/python3

import http.server
import socketserver
import time
import threading
import json
import requests

import sys
import logging
import logging.handlers

LOG_FILENAME = "middleware.log"

middleware_logger = logging.getLogger('middleware_logger')
middleware_logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
rotating_file_handler = logging.handlers.RotatingFileHandler(LOG_FILENAME, maxBytes=10*1024*1024, backupCount=5)
rotating_file_handler.setLevel(logging.DEBUG)
rotating_file_handler.setFormatter(formatter)
middleware_logger.addHandler(rotating_file_handler)
stdout_handler = logging.StreamHandler(sys.stdout)
stdout_handler.setLevel(logging.INFO)
stdout_handler.setFormatter(formatter)
middleware_logger.addHandler(stdout_handler)

DRONEBOX_IP = "http://169.254.246.1:80"
USERNAME = "engineer" # API KEY
PASSWORD = "123"
# USERNAME = "hus_admin"
# PASSWORD = "husfly123"
LISTENING_PORT = 8000
EMULATOR_IP = "http://localhost:8001"
IS_EMULATE = False

DEPLOY_TIME = 10
STOW_TIME = 10
CHARGE_TIME = 60.0
STATE_UPDATE_PERIOD = 0.5

class DeployStep():
    INITIALIZED = 0
    TURNING_OFF_CHARGER = 1
    OPENING_HATCHES = 2
    RAISING_LIFT = 3
    OPENING_RING = 4 # centering ring to home position
    WAITING_FOR_DEPLOY = 5 # automatically transitions to LandStep = 5
    DRONE_LAUNCHED = 6

class LandStep():
    INITIALIZED = 0
    WAITING_FOR_LANDING = 5
    CLOSING_RING = 6 # centering ring to position 1
    SPINNING_DISK = 7 # polarity check
    SQUEEZING_RING = 8 # centering ring to position 2
    LOWERING_LIFT = 9
    CLOSING_HATCHES = 10
    TURNING_ON_CHARGER = 11 # automatically transitions to LandStep = 0, DeployStep = 0

class DroneboxState():
    DEPLOYING = "DEPLOYING"
    DEPLOYED = "DEPLOYED"
    STOWING = "STOWING"
    CHARGING = "CHARGING"
    READY = "READY"
    ERROR = "ERROR"

class ResponseType():
    DONE = "DONE"
    WAIT = "WAIT"
    ERROR = "ERROR"

class Dronebox():
    def __init__(self):
        self.state_lock = threading.RLock()
        self.state = DroneboxState.READY
        self.charge_time_remaining_s = 0.0
        self.update_thread = self.StateUpdateThread(self)
        self.update_thread.start()

    def deploy(self):
        middleware_logger.info("deploy()")
        with self.state_lock:
            if self.state == DroneboxState.READY:
                if not self.write("Deploy_Start", 1):
                    # self.setState(DroneboxState.ERROR)
                    return self.makeResponse(ResponseType.ERROR, "Deploy failed")
                else:
                    return self.makeResponse(ResponseType.WAIT, DEPLOY_TIME)
            elif self.state == DroneboxState.DEPLOYING:
                return self.makeResponse(ResponseType.WAIT, DEPLOY_TIME)
            elif self.state == DroneboxState.DEPLOYED:
                return self.makeResponse(ResponseType.DONE, str(DroneboxState.DEPLOYED))
            else:
                return self.makeResponse(ResponseType.ERROR, "Cannot deploy in state: " + str(self.state))

    def stow(self):
        middleware_logger.info("stow()")
        with self.state_lock:
            if self.state == DroneboxState.DEPLOYED:
                if not self.write("Drone_Landed", 1):
                    # self.setState(DroneboxState.ERROR)
                    return self.makeResponse(ResponseType.ERROR, "Stow failed")
                else:
                    self.charge_time_remaining_s = CHARGE_TIME
                    return self.makeResponse(ResponseType.WAIT, STOW_TIME)
            elif self.state == DroneboxState.STOWING:
                return self.makeResponse(ResponseType.WAIT, STOW_TIME)
            elif self.state == DroneboxState.CHARGING or self.state == DroneboxState.READY:
                return self.makeResponse(ResponseType.DONE, str(self.state))
            else:
                return self.makeResponse(ResponseType.ERROR, "Cannot stow in state: " + str(self.state))

    def setState(self, to_state):
        with self.state_lock:
            self.state = to_state

    def makeResponse(self, response_type, message):
        middleware_logger.debug("makeResponse(" + str(response_type) + ", " + str(message) + ")")
        return json.dumps((response_type, message))

    def getStateString(self):
        with self.state_lock:
            state = self.state
        state_string = json.dumps(('STATE', state))
        middleware_logger.debug("getStateString() returns " + str(state_string))
        return state_string

    def write(self, variable_name, value):
        payload = {'value' : value}
        if IS_EMULATE:
            r = requests.post(str(EMULATOR_IP) + "/" + str(variable_name), data = json.dumps(payload), verify = False)
        else:
            r = requests.post(str(DRONEBOX_IP) + "/api/v1/device/strategy/vars/int32s/" + str(variable_name), data = json.dumps(payload), auth=(USERNAME, PASSWORD), verify=False)
        middleware_logger.info("Returning " + str(variable_name) + ":" + str(value))
        middleware_logger.info(r.text)
        return r.status_code == 200

    def read(self, variable_name):
        if IS_EMULATE:
            r = requests.get(str(EMULATOR_IP) + "/" + str(variable_name), verify=False)
        else:
            r = requests.get(str(DRONEBOX_IP) + "/api/v1/device/strategy/vars/int32s/" + str(variable_name), auth=(USERNAME, PASSWORD), verify=False)
        value = (json.loads(r.text))['value']
        return value

    def stop(self):
        middleware_logger.info("Stopping dronebox")
        try:
            with self.state_lock:
                if self.update_thread.is_alive():
                    middleware_logger.info("Stopping update_thread")
                    self.update_thread.is_continue = False
                    self.update_thread.join()
                    middleware_logger.info("Stopped update_thread")
        except:
            pass
        middleware_logger.info("Stopped dronebox")

    class StateUpdateThread(threading.Thread):
        def __init__(self, dronebox):
            super().__init__()
            self.dronebox = dronebox
            self.is_continue = True
            self.last_time = None

        def run(self):
            while self.is_continue:
                with self.dronebox.state_lock:
                    deploy_step = self.dronebox.read("Autonomous_Deploy_Step")
                    land_step = self.dronebox.read("Autonomous_Landing_Step")

                    # if self.dronebox.state == DroneboxState.ERROR:
                        # continue

                    if deploy_step == DeployStep.INITIALIZED and land_step == LandStep.INITIALIZED:
                        if self.dronebox.charge_time_remaining_s == 0.0:
                            self.last_time = None
                            self.dronebox.setState(DroneboxState.READY)
                        else:
                            time_elapsed = 0
                            if self.last_time != None:
                                time_elapsed = time.time() - self.last_time
                            self.last_time = time.time()
                            self.dronebox.charge_time_remaining_s = max(0.0, self.dronebox.charge_time_remaining_s - time_elapsed)
                            self.dronebox.setState(DroneboxState.CHARGING)
                            middleware_logger.info("Charge time remaining: " + str(self.dronebox.charge_time_remaining_s))
                    elif DeployStep.INITIALIZED < deploy_step < DeployStep.WAITING_FOR_DEPLOY:
                        self.dronebox.setState(DroneboxState.DEPLOYING)
                    elif deploy_step >= DeployStep.WAITING_FOR_DEPLOY or land_step == LandStep.WAITING_FOR_LANDING:
                        self.dronebox.setState(DroneboxState.DEPLOYED)

                    if LandStep.WAITING_FOR_LANDING < land_step < LandStep.TURNING_ON_CHARGER:
                        self.dronebox.setState(DroneboxState.STOWING)

                    middleware_logger.info("state: " + str(self.dronebox.state) + ", deploy_step: " + str(deploy_step) + ", land_step: " + str(land_step))
                time.sleep(STATE_UPDATE_PERIOD)

class DroneboxRequestHandler(http.server.SimpleHTTPRequestHandler):
    def do_POST(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        length = int(self.headers.get('content-length'))
        post_body = self.rfile.read(length)
        post_string = bytes.decode(post_body, "utf-8")

        dronebox = self.server.dronebox
        request = (json.loads(post_string))['request']
        if (request == "deploy"):
            middleware_logger.info("deploy received from " + str(self.client_address))
            response_string = dronebox.deploy()
        elif (request == "stow"):
            middleware_logger.info("stow received from " + str(self.client_address))
            response_string = dronebox.stow()
        elif (request == "state"):
            middleware_logger.info("state received from " + str(self.client_address))
            response_string = dronebox.getStateString()
        else:
            middleware_logger.info("Bad request: " + str(post_string) + " from " + str(self.client_address))
            return
        self.wfile.write(bytes(response_string, "utf-8"))
        middleware_logger.info("Response: " + response_string + " sent!")

    def log_message(self, format, *args):
        return

class DroneboxServer(socketserver.TCPServer):
    def __init__(self):
        super().__init__(("", LISTENING_PORT), DroneboxRequestHandler)
        # self.allow_reuse_address = True
        self.dronebox = Dronebox()
        threading.Thread(target=self.serve_forever).start()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        middleware_logger.info("Stopping server")
        self.shutdown()
        self.server_close()
        self.dronebox.stop()
        middleware_logger.info("Stopped server")

with DroneboxServer():
    while (True):
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            middleware_logger.info("KeyboardInterrupt!")
            break
middleware_logger.info("Exiting...")
