import requests
import json

DRONEBOX_IP = "http://169.254.246.20:443"
USERNAME = "wongrufus"
PASSWORD = "1234"

payload = {'value' : 1}
r = requests.post(str(DRONEBOX_IP) + "/api/v1/device/strategy/vars/int32s/Autonomous_Mode_On_HMI", data = json.dumps(payload), auth=(USERNAME, PASSWORD), verify=False)
print(r.text)
