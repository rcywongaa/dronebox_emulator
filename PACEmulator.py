import http.server
import socketserver
import time
import threading
import json
import requests
from urllib.parse import urlparse
import posixpath

PORT = 8001
STEP_TIME_S = 5

class DeployStep():
    INITIALIZED = 0
    TURNING_OFF_CHARGER = 1
    OPENING_HATCHES = 2
    RAISING_LIFT = 3
    OPENING_RING = 4 # centering ring to home position
    WAITING_FOR_DEPLOY = 5 # automatically transitions to LandStep = 5
    DRONE_LAUNCHED = 6

class LandStep():
    INITIALIZED = 0
    WAITING_FOR_LANDING = 5
    CLOSING_RING = 6 # centering ring to position 1
    SPINNING_DISK = 7 # polarity check
    SQUEEZING_RING = 8 # centering ring to position 2
    LOWERING_LIFT = 9
    CLOSING_HATCHES = 10
    TURNING_ON_CHARGER = 11 # automatically transitions to LandStep = 0, DeployStep = 0

class PACEmulator():
    def __init__(self):
        self.variables = {
                "Deploy_Start":0,
                "Drone_Landed":0,

                "Autonomous_Deploy_Step":0,
                "Autonomous_Landing_Step":0
        }
        self.lock = threading.Lock()
        self.update_thread = PACEmulator.UpdateThread(self)
        self.update_thread.start()

    def stop(self):
        try:
            if self.update_thread.is_alive():
                print("Stopping update_thread")
                self.update_thread.is_continue = False
                self.update_thread.join()
                print("Stopped update_thread")
        except:
            pass

    class UpdateThread(threading.Thread):
        def __init__(self, pac):
            super().__init__()
            self.pac = pac
            self.is_continue = True
            self.last_step_time = 0

        def run(self):
            while self.is_continue:
                with self.pac.lock:
                    if self.pac.variables['Deploy_Start'] == 1 and time.time() - self.last_step_time >= STEP_TIME_S:
                        self.last_step_time = time.time()
                        self.pac.variables['Autonomous_Deploy_Step'] = self.pac.variables['Autonomous_Deploy_Step'] + 1
                        if self.pac.variables['Autonomous_Deploy_Step'] >= DeployStep.WAITING_FOR_DEPLOY:
                            self.pac.variables['Autonomous_Deploy_Step'] = 0
                            self.pac.variables['Deploy_Start'] = 0
                            self.pac.variables['Autonomous_Landing_Step'] = LandStep.WAITING_FOR_LANDING
                    elif self.pac.variables['Drone_Landed'] == 1 and time.time() - self.last_step_time >= STEP_TIME_S:
                        self.last_step_time = time.time()
                        self.pac.variables['Autonomous_Landing_Step'] = self.pac.variables['Autonomous_Landing_Step'] + 1
                        if self.pac.variables['Autonomous_Landing_Step'] >= LandStep.TURNING_ON_CHARGER:
                            self.pac.variables['Autonomous_Landing_Step'] = 0
                            self.pac.variables['Drone_Landed'] = 0
                time.sleep(0.5)

class PACRequestHandler(http.server.SimpleHTTPRequestHandler):
    def do_POST(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        length = int(self.headers.get('content-length'))
        post_body = self.rfile.read(length)
        post_string = bytes.decode(post_body, "utf-8")

        variable = posixpath.basename(urlparse(self.path).path)
        value = (json.loads(post_string))['value']

        try:
            with self.server.pac.lock:
                self.server.pac.variables[variable] = value
                print(str(variable) + " set to " + str(value))
        except:
            print("Variable: " + str(variable) + " not found")

    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()

        variable = posixpath.basename((urlparse(self.path)).path)

        try:
            with self.server.pac.lock:
                value = self.server.pac.variables[variable]
                print(str(variable) + " = " + str(value) + " returned")
        except:
            print("Variable: " + str(variable) + " not found")

        self.wfile.write(bytes(json.dumps({'value' : value}), "utf-8"))

    def log_message(self, format, *args):
        return

class PACServer(socketserver.TCPServer):
    def __init__(self):
        super().__init__(("", PORT), PACRequestHandler)
        self.pac = PACEmulator()
        threading.Thread(target=self.serve_forever).start()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        print("Stopping PAC server")
        self.pac.stop()
        self.shutdown()
        self.server_close()
        print("Stopped PAC server")

with PACServer():
    while (True):
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            print("KeyboardInterrupt!")
            break
print("Exiting...")
