#!/usr/bin/python3

import requests
import json

while (True):
    command = input()
    payload = {'request':command}

    r = requests.post('http://nuc.smbox.co:8000', data=json.dumps(payload))
    print(r.text)
